'use strict';

app.factory('tumblrService', ['$http',
	function($http) {
		
		var base = 'https://api.tumblr.com';
		var api_key = 'A21rSfzaXnWdh9zWBkP7OeNfGu852RHA9rftDDKxHjD6YdUXKh';
		return {
			'get': function() {
				var request = '/v2/blog/michaelsmosleydotcom.tumblr.com/posts';
				var url = base + request;
				var config = {
					'params': {
						'api_key': api_key,
						'limit':50,
						'callback': 'JSON_CALLBACK'
					}
				};
				return $http.jsonp(url, config);
			}

			

		};
	}
]);