'use strict';

app.factory('Post', function($firebase, FIREBASE_URL) {
  //var ref = new Firebase(FIREBASE_URL + 'posts');
  //var posts = $firebase(ref);
  //console.dir(posts);
  var Post = {
    getType: function (category) {
        var thisref = new Firebase(FIREBASE_URL + category);
        var thisposts = $firebase(thisref);
        return thisposts;


    },
    create: function (post,category) {
      var thisref = new Firebase(FIREBASE_URL + category);
      var thisposts = $firebase(thisref);
        return thisposts.$add(post).then(function (ref) {
          var postId = ref.name();
          return postId;
        });
    }
  };

  return Post;
});