'use strict';
app.factory('Category', function($firebase, FIREBASE_URL) {
  var ref = new Firebase(FIREBASE_URL + 'categories');
  var categories = $firebase(ref);

  var Category = {
    all: categories,
    create: function (post) {
        return posts.$add(post).then(function (ref) {
          var postId = ref.name();
          return postId;
        });
    }
  };

  return Category;
});