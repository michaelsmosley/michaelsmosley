'use strict';

app.factory('srvLibrary', ['$http',
	function($http) {
		
		var base = 'https://api.instagram.com/v1';
		// get your own client id http://instagram.com/developer/
		var clientId = '0c59d2e84d0c4d36b15758c0a98c3320';
		return {
			'get': function(count, hashtag) {
				var request = '/users/'+hashtag+'/media/recent/';
				//var request = '/tags/' + hashtag + '/media/recent';
				var url = base + request;
				var config = {
					'params': {
						'client_id': clientId,
						'count': count,
						'callback': 'JSON_CALLBACK'
					}
				};
				return $http.jsonp(url, config);
			}

			

		};
	}
]);