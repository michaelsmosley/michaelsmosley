/* global app:true */

'use strict';
console.log("TTTTT");
var app = angular.module('michaelsmosleycomApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngFitText',
  'firebase',
  'ui.router'
]);

app.config(function($stateProvider,$urlRouterProvider) {

  console.log("appconfig");
  $urlRouterProvider.when('', '/category/home/0/main');

  $stateProvider
  .state('categories', {
      url: '/category',
      templateUrl: 'views/instagram.html',
      controller: 'InstgramCtrl',
      resolve: {
        instagramdata: function(srvLibrary) {
          return srvLibrary.get(8, 10619015);
        },
        tumblrdata: function(tumblrService) {
          return tumblrService.get();
        },
        facebooktoken: function(facebookService) {
            return facebookService.getToken();
        },
        photos: function(photosService) {
            return photosService.getPhotos();
        }
      },
      reloadOnSearch: false

  })
  .state('categories.categoryview', {
    resolve: {
        facebookdata: function(facebookService,facebooktoken) {
          var authtoken=facebooktoken.data[1];
         
          return facebookService.getData(authtoken);
        }
    },
    views: {
      
      'showall': {
        templateUrl: 'views/showall.html',
        controller: 'ShowAllCtrl'
      },
      'links': {
        templateUrl: 'views/links.html',
        controller: 'LinksCtrl'
      },
      'facebook': {
        templateUrl: 'views/facebook.html',
        controller: 'FacebookCtrl'
      },
      'slidecontent': {
            templateUrl: 'views/category.html',
            controller: 'CategoryCtrl'
      },
      'photos': {
            templateUrl: 'views/category.html',
            controller: 'PhotosCtrl'
      },
      'video': {
            templateUrl: 'views/category.html',
            controller: 'VideoCtrl'
      },
      'experiments': {
            templateUrl: 'views/category.html',
            controller: 'ExperimentsCtrl'
      },
      'resume': {
            templateUrl: 'views/category.html',
            controller: 'ResumeCtrl'
      },
      'work': {
            templateUrl: 'views/category.html',
            controller: 'WorkCtrl'
      },
      'home': {
            templateUrl: 'views/category.html',
            controller: 'CategoryCtrl'
      }
    },
    url: '/:category/:slide/:view'

  })
   
  .state('moderate', {
    url: '/moderate',
      templateUrl: 'views/moderate.html',
      controller: 'ModerateCtrl'
  });





/*

,
        
,
        facebookdata: function(facebookService) {
            return facebookService.get();
        }


  $routeProvider
    .when('/', {
      templateUrl: 'views/instagram.html',
      controller: 'InstgramCtrl',
      reloadOnSearch: false,
      resolve: {
        instagramdata: function(srvLibrary) {
          return srvLibrary.get(6, 10619015);
        }

      }
    })
    .when('/category:catId', {
      templateUrl: 'views/instagram.html',
      controller: 'InstgramCtrl',
      reloadOnSearch: false,
      resolve: {
        instagramdata: function(srvLibrary) {
          return srvLibrary.get(6, 10619015);
        }

      }
    })
    .when('/cat', {
      templateUrl: 'views/category.html',
      reloadOnSearch: false,
      controller: 'CategoryCtrl'
    })
    .when('/moderate', {
      templateUrl: 'views/moderate.html',
      controller: 'CategoryCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });*/
});

app.constant('FIREBASE_URL', 'https://michaelsmosleydotcom.firebaseio.com/');