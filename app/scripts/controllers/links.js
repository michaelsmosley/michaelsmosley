'use strict';

angular.module('michaelsmosleycomApp')
  .controller('LinksCtrl', ['$scope',function ($scope) {
    $scope.linkslist=[{id:"bitbucket",url:"https://bitbucket.org/michaelsmosley/michaelsmosley/src", copy:"git repo for this site!"},
      {id:"resume",url:"http://docs.google.com",copy:"see my google doc resume!"},
      {id:"facebook",url:"https://www.facebook.com/michaelstephenmosley",copy:"check me out on facebook"},
      {id:"twitter",url:"https://twitter.com/michaelmosley",copy:"i don't really tweet! but check me out anyway."},
      {id:"tumblr",url:"http://michaelsmosley.tumblr.com/",copy:"i might tumlbr, sometimes"},
      {id:"map",url:"http://www.bing.com/maps/#JnE9LmNoaW5hdG93biUyNTJjJTJibmV3JTJieW9yayU3ZXNzdC4wJTdlcGcuMSZiYj02NC4zMDg0OTI0OTE1OTU4JTdlLTI5LjMzMDU4MTY2NSU3ZTQuNzEyMjExNjMwNTkyMjElN2UtMTE4LjYyNzQ1NjY2NQ==",copy:"see where i live"},
      {id:"instagram",url:"http://instagram.com/michaelsmosley",copy:"my pics on instagram"},
      {id:"email",url:"mailto:michaelsmosley@gmail.com",copy:"why don't you email me?"}
    ]

    $scope.openLink=function (slide) {
      console.log("openLink");
      $scope.navigateToURL($scope.linkslist[slide].url)
    }
    $scope.showInfo=function(e,slide) {
        $("#linksarrow").css('opacity',1);
        $("#linksarrow").css('top',e.pageY-$("#linksarrow").height()-40);
        $("#linksarrow").css('left',e.pageX-$("#linksarrow").width()/2-10);
        $("#linksarrow").html($scope.linkslist[slide].copy);
   
      }
      $scope.hideInfo=function() {
        $("#linksarrow").css('opacity',0);
        $("#linksarrow").css('top',-1000);
      }


  }]);


