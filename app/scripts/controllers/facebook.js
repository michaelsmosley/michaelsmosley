'use strict';

angular.module('michaelsmosleycomApp')
  .controller('FacebookCtrl', ['$scope', 'facebookdata',
  	function ($scope,facebookdata) {
  		//$scope.authtoken=authtoken;
  		//var authtoken=facebookdata.data[1];
 		console.log("controller comes in "+facebookdata);
  		/*facebookService.getData(authtoken).then(function (postId) {
          $scope.facebookdata=postId.data.data;
          $scope.totalFB=postId.data.data.length;
          console.log("$scope.totalFB"+$scope.totalFB);
        });*/
		$scope.facebookdata=facebookdata.data.data;
        $scope.totalFB=facebookdata.data.data.length;
  		$scope.currentFB=0;

		$scope.setFBClass=function (catid) {
		      var returnval='fbpost notselectedfb';  
		      if (catid == $scope.currentFB) {
		          returnval='fbpost selectedfb';	          
		      }
		      return returnval;
	    }
	    $scope.nextFB=function () {
		      
		      if ($scope.currentFB<$scope.totalFB-1) {
		      	$scope.currentFB++;
		      } else {
		      	$scope.currentFB=0;
		      }

	    }

	    $scope.relativeTime=function(dateObj) {
			var date=new Date(dateObj)
			var delta = new Date() - date;
			var now_threshold = 5000;
			if (delta <= now_threshold) {
				return 'JUST NOW';
			}
			var units = null, conversions = {
				millisecond: 1,
				second: 1000,
				minute: 60,
				hour: 60,
				day: 24,
				month: 30,
				year: 12
			};

			for (var key in conversions) {
				if (delta < conversions[key]) {
					break;
				} else {
					units = key;
					delta = delta / conversions[key];
				}
			}
			delta = Math.floor(delta);
			if (delta) {
				if (delta !== 1) { units += "s"; }
				return [delta, units, " ago "].join(" ");
			} else {
				return " ";
			}
		};
		$scope.formatMsg=function(msg,data) {
			var tempmsg=msg;
			
			if (data.link) {
				if (tempmsg.indexOf("a link")>=0) {
					tempmsg=tempmsg.replace("link","<a target=\"_blank\" href="+data.link+">"+"link"+"</a>");
				} else {
					if (tempmsg.indexOf("http://")>=0) {
						var msgsplit=tempmsg.split("http://");
						var testsplit=msgsplit[1].split(" ")[0];
						var spliturl="http://"+testsplit;
						tempmsg=tempmsg.replace(spliturl,"<a target=\"_blank\" href='"+spliturl+"'>"+"[link]"+"</a>");
					}
				}
			}
			if (data.type=="status") {
				var searchterm="is going to";
				if (tempmsg.indexOf("is going to")>=0 || tempmsg.indexOf("went to")>=0) {
					if (tempmsg.indexOf("went to")>=0) {
						searchterm="went to";
					}
					var msgsplit=tempmsg.split(searchterm);
					var testsplit=msgsplit[1].split(" &mdash;")[0];
					tempmsg=tempmsg.replace(testsplit,"<a target=\"_blank\" href='"+data.actions[0].link+"'>"+testsplit+"</a>");
				}
			}
			return tempmsg;
		}

  		$scope.formatMessage=function(data) {
  				var msg="coming soon!"

				if (data.story) {
					var tempmsg=data.story;
					if (data.message) {
						tempmsg+="<br/><br/><div id='smallmsg'> \""+data.message+"\"</div>";
					}
					if (data.story_tags) {
						for (var key in data.story_tags) {
						   var obj = data.story_tags[key];
						   for (var prop in obj) {
								var userobj=obj[prop];
								var userid=userobj.id;
								var username=userobj.name;
								tempmsg=$scope.formatMsg(tempmsg,data);
								tempmsg=tempmsg.replace(username,"<a target=\"_blank\"  href=https://www.facebook.com/"+userid+">"+username+"</a>");				
						   }
						}
						if (data.type=="photo") {
							tempmsg=tempmsg.replace("photo","<a target=\"_blank\" href="+data.link+">"+"photo"+"</a>");
						}
						msg=tempmsg;
					} else {
						msg=tempmsg;
					}
				} else if (data.message) {
					if (data.status_type=="added_photos") {
						msg=data.from.name+" tagged a ";
						msg+=" <a target=\"_blank\" href='"+data.link+"'>"+"photo"+"</a>";
					} else {
						msg=$scope.formatMsg(data.message,data);
						if (data.from.name != "Michael Mosley" ) {
							var fromurl="https://www.facebook.com/profile.php?id="+data.from.id;
							msg = "<a target=\"_blank\" href='"+fromurl+"'>"+data.from.name+"</a> wrote: \""+msg+"\"";
						}
						if (data.type == "link" ) {
							msg += "  <a target=\"_blank\" href='"+data.link+"'>"+data.link+"</a>";
						}
					}
				} else if (data.link) {
					msg=data.from.name+" liked ";
					msg += "  <a target=\"_blank\" href='"+data.link+"'>"+data.description+"</a>";
				} 

				var statusHTML='<div class=\'timeago\'>'+$scope.relativeTime(data.created_time) + '</div>'; 

				return msg+statusHTML;
  		}




  }]);