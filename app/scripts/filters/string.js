'use strict';
 
app.filter('firstCharacter', function () {
  return function (str) {
    return str.charAt(0)
  };
});