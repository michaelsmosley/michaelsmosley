'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('michaelsmosleycomApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(4);
  });

  it('should have a var with hello world called txt', function(){
    console.log('test');
    expect(scope.txt).toBe('Hello World!');
  });

});
